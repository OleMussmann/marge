# marge

Markdown Renderer (Github Edition)

Converts a markdown file to html in Github CSS style, starts a web server and serves the html file in your favourite browser. Watches the markdown file for changes and refreshes the html file, thus allowing for near-live markdown editing. No internet access required.

Inspired by [grip](https://github.com/joeyespo/grip).

## Usage
```
Usage: marge [OPTIONS]... INPUT_FILE [OUTPUT_FILE]

Converts a markdown INPUT_FILE to html and serves it with a web server.
Auto-refreshes on INPUT_FILE changes. Writes a standalone OUTPUT_FILE if
requested. This is an early version, command line arguments are subject
to change.

  -h, --help          display this help message
  -p, --port [PORT]   webserver port (default: 8000)
  -s, --single-run    render once and exit, requires OUTPUT_FILE
  -v, --version       print version information
  -w, --write-only    render file without web server, requires OUTPUT_FILE
```

## Installation
`marge` is available in several stores, in various formats. The following install options are sorted in ascending complexity, but not all may work for your system.

<details><summary><b>Snap</b></summary>
The easiest solution for is installing it from the snap store. This way it will also be kept up-to-date via snap.

#### Store
Install it from Ubuntu Software or

[![Get it from the Snap Store](https://snapcraft.io/static/images/badges/en/snap-store-white.svg)](https://snapcraft.io/marge)

or install from the command line:
```
snap install marge
```

##### Uninstall
Uninstall it via Ubuntu Software or on the command line:
```
sudo snap remove marge
```

#### Snap file
Will not be automatically updated. You have to check for new versions yourself.
```
wget https://gitlab.com/OleMussmann/marge/raw/master/build/snap/marge_0.4.1_amd64.snap
sudo snap install --dangerous marge_0.4.1_amd64.snap
```

##### Uninstall
Uninstall it via Ubuntu Software or on the command line:
```
sudo snap remove marge
```
</details>

<details><summary><b>Deb</b></summary>

If snap is not available, a marge `*.deb` file can be installed from the PPA (Personal Package Archive) or by hand.

#### PPA
Using the PPA will keep it automatically updated with the rest of your system.
```
sudo add-apt-repository ppa:ole-1/ppa
sudo apt update
```
Now either search for it in Ubuntu Software, or install it from the command line:
```
sudo apt install marge
```

##### Uninstall
Uninstall it via Ubuntu Software or on the command line:
```
sudo apt remove marge
```
If set up the PPA and you want to purge it, you can use `ppa-purge`:
```
sudo apt install ppa-purge
ppa-purge ppa:ole-1/ppa
```

#### Deb file
Without access to either PPAs or snap, you can install a `*.deb` file by hand. Not kept up-to-date.
```
wget https://gitlab.com/OleMussmann/marge/raw/master/build/deb/marge_0.4.1~xenial1_all.deb
sudo dpkg -i marge_0.4.1~xenial1_all.deb
```

##### Uninstall
```
sudo dpkg -r marge
```
</details>

<details><summary><b>Direct</b></summary>

Install dependencies:
```
sudo apt install inotify-tools pandoc busybox
```
Clone this repository:
```
git clone git@gitlab.com:OleMussmann/marge.git
```
Get the latest version by regularly doing a fresh `git pull` in the marge folder.

Don't have git? Download the tarball instead. Unzip it anywhere and remove the tarball. A tarball installation is not automatically kept up-to-date.
```
wget https://gitlab.com/OleMussmann/marge/raw/master/build/tar_gz/marge_0.4.1~1.tar.gz
tar xf marge_0.4.1~1.tar.gz
rm marge_0.4.1~1.tar.gz
```
Use the executable `marge` directly, or include the containing folder in your path to access it from anywhere on your system.

##### Uninstall
Just remove the folder. Remove the folder from your path if you added it before.
```
rm -rf marge
```
</details>

<details><summary><b>MacOS</b></summary>

If necessary, install [homebrew](https://brew.sh/) first:
```
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```
Then install the dependencies via `brew`:
```
brew install pandoc fswatch coreutils
```
Clone this repository:
```
git clone git@gitlab.com:OleMussmann/marge.git
```
Get the latest version by regularly doing a fresh `git pull` in the marge folder.

Don't have git? Download the tarball instead. Unzip it anywhere and remove the tarball. A tarball installation is not automatically kept up-to-date.
```
wget https://gitlab.com/OleMussmann/marge/raw/master/build/tar_gz/marge_0.4.1~1.tar.gz
tar xf marge_0.4.1~1.tar.gz
rm marge_0.4.1~1.tar.gz
```
Use the executable `marge` directly, or include the containing folder in your path to access it from anywhere on your system.

##### Uninstall
Just remove the folder. Remove the folder from your path if you added it before.
```
rm -rf marge
```

</details>

## Building
Not necessary for using marge, but feel free to play with it. `git clone` the code and use one of the provided build scripts.

```
git clone git@gitlab.com:OleMussmann/marge.git
```

### Snap
```
build/snap/01_build_snap.sh
```

### Deb
You should set the `DEBMAIL` and `DEBFULLNAME` environment variables before building a `*.deb` file. Append this code snippet to your .bashrc:

```
DEBEMAIL="your@emailaddress.com"
DEBFULLNAME="your name"
export DEBEMAIL DEBFULLNAME
```

If you want to sign the package, you need to [set up a GPG key](https://help.launchpad.net/YourAccount/ImportingYourPGPKey). Creating the package happens in two steps, preparation and building.

```
build/deb/01_prepare_deb.sh
```
Edit `marge-*/debian/changelog` and supply a meaningfull changelog message.
```
build/deb/02_build_deb.sh
```

### Tarball
```
build/tar_gz/01_build_tar_gz.sh
```
