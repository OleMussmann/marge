# Changelog
All notable changes to this project will be documented in this file.

## [Unreleased]
### Added
- Example markdown file.
- Option for different CSS styles.
- Follow links: compile a document tree
    if markdown file contains links to other markdown files.

## [0.4.1] - 2018-12-01
### Fixed
- Task list not triggering on list with links anymore.

## [0.4] - 2018-12-01
### Added
- Mac support

## [0.3.2.1] - 2018-11-28
### Fixed
- Reduce snap size.

## [0.3.2] - 2018-11-28
### Added
- Supporting Github task lists

### Fixed
- Spacing at bottom of page

## [0.3.1] - 2018-11-22
### Fixed
- Reduce snap size.

## [0.3] - 2018-11-22
### Fixed
- Inotify now triggers on all modifications. Fixes #1 .

## [0.2] - 2018-11-19
### Added
- Use xdg-open to open rendered html in your favourite browser.

### Fixed
- Create sub-folder if target file is not in current directory.
- Improved comments in shell script.

### Changed
- renamed command line options
  - `-o/--once` -> `-s/--single-run`

## [0.1] - 2018-11-18
### Added
- First commit.
