#!/usr/bin/env bash

BASE_FOLDER="$(dirname $(realpath $BASH_SOURCE))/../.."
VERSION=$(cat $BASE_FOLDER/VERSION)
REVISION=$(cat $BASE_FOLDER/REVISION)
FILENAME="marge_"$VERSION"~"$REVISION".tar.gz"
FILE_LIST="
marge/CHANGELOG.md 
marge/files 
marge/LICENSE 
marge/marge 
marge/NOTICE 
marge/README.md 
marge/REVISION
marge/VERSION"

tar -C $BASE_FOLDER/.. --create \
  --file $BASE_FOLDER/build/tar_gz/$FILENAME $FILE_LIST
