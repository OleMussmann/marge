#!/usr/bin/env bash

BASE_FOLDER="$(dirname $(realpath $BASH_SOURCE))/../.."

cd $BASE_FOLDER
snapcraft clean && snapcraft && snapcraft clean
mv $BASE_FOLDER/*.snap build/snap/
