#!/usr/bin/env bash

set -e

BASE_FOLDER="$(dirname $(realpath $BASH_SOURCE))/../.."
VERSION=$(cat $BASE_FOLDER/VERSION)
REVISION=$(cat $BASE_FOLDER/REVISION)
DISTRIBUTION=$(cat $BASE_FOLDER/meta/DISTRIBUTION)

usage(){
  echo "Usage: $(basename $BASH_SOURCE) [OPTIONS]"
  echo
  echo "  -c, --cleanup MODE  delete build files after building (default: true)"
  echo "                        true: delete build files"
  echo "                        false: don't delete build files"
  echo "                        only: only clean up, don't build"
  echo "  -h, --help          display this help message"
  echo "  -t, --target TARGET build mode (default: deb)"
  echo "                        'deb' builds a *.deb package for" \
    "local installation"
  echo "                        'ppa' builds a *.changes file for" \
    "uploading to ppa"
}

cleanup(){
  set +e
  cd $BASE_FOLDER
  echo
  echo "Cleaning up build files ..."
  rm -r marge-*
  mv marge_*.build build/deb/
  mv marge_*.buildinfo build/deb/
  mv marge_*.changes build/deb/
  mv marge_*.tar.xz build/deb/
  mv marge_*.dsc build/deb/
  mv marge_*.deb build/deb/
  echo "... done."
}

CLEANUP=true
TARGET="deb"

while true
do
  case $1 in
    -c | --cleanup )  shift; CLEANUP=$1; shift; ;;
    -h | --help )     usage; exit ;;
    -t | --target )   shift; TARGET=$1; shift; ;;
    * )               break ;;
  esac
done

if [[ $CLEANUP == "only" ]]
then
  cleanup
  exit
fi

if [[ $DEBEMAIL == "" || $DEBFULLNAME == "" ]]
then
  echo "Environment variables DEBEMAIL and DEBFULLNAME" \
    "have to be set for building."
  exit 1
fi

if [[ $TARGET == "ppa" ]]
then
  STANDALONE_DEB=false
elif [[ $TARGET == "deb" ]]
then
  STANDALONE_DEB=true
else
  usage; exit 1
fi

cd $BASE_FOLDER/marge-$VERSION~$DISTRIBUTION$REVISION

if [[ $STANDALONE_DEB == true ]]
then
  debuild
else
  debuild -S
fi

if [[ $STANDALONE_DEB == true ]]
then
  cd $BASE_FOLDER
  cp marge_*.deb build/deb/
  cd -
fi

if [[ $CLEANUP == true ]]
then
  cleanup
fi
