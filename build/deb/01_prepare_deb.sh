#!/usr/bin/env bash

if [[ $DEBEMAIL == "" || $DEBFULLNAME == "" ]]
then
  echo "Environment variables DEBEMAIL and DEBFULLNAME" \
    "have to be set for building."
  exit 1
fi

BASE_FOLDER="$(dirname $(realpath $BASH_SOURCE))/../.."
VERSION=$(cat $BASE_FOLDER/VERSION)
REVISION=$(cat $BASE_FOLDER/REVISION)
DISTRIBUTION=$(cat $BASE_FOLDER/meta/DISTRIBUTION)
HOMEPAGE=$(cat $BASE_FOLDER/meta/HOMEPAGE)
DESCRIPTION_SHORT=$(head -n 3 $BASE_FOLDER/README.md | tail -n 1)
DESCRIPTION_LONG=$(head -n 5 $BASE_FOLDER/README.md | tail -n 1)
DEPENDENCIES="xdg-utils, pandoc, inotify-tools"
FILE_LIST="CHANGELOG.md files LICENSE marge \
  NOTICE README.md REVISION VERSION"

INSTALL="marge usr/bin\n\
marge.sh etc/marge\n\
CHANGELOG.md etc/marge\n\
files etc/marge\n\
LICENSE etc/marge\n\
marge etc/marge\n\
NOTICE etc/marge\n\
README.md etc/marge\n\
REVISION etc/marge\n\
VERSION etc/marge"

cd $BASE_FOLDER

mkdir "marge-$VERSION~$DISTRIBUTION$REVISION"
cp -r $FILE_LIST "marge-$VERSION~$DISTRIBUTION$REVISION"
cd "marge-$VERSION~$DISTRIBUTION$REVISION"

mv marge marge.bak
cat marge.bak | \
sed -e "s|^PACKAGE_NAME=.*|PACKAGE_NAME=\"marge\"|" | \
sed -e "s|^VERSION=.*|VERSION=\"$VERSION\"|" | \
sed -e "s|^REVISION=.*|REVISION=\"$REVISION\"|" | \
sed -e "s|^ARCH=.*|ARCH=\"all\"|" | \
sed -e "s|^PACKAGE_ROOT=.*|PACKAGE_ROOT=\"/etc/marge/\"|" | \
sed -e "s|^FS_ROOT=.*|FS_ROOT=\"/\"|" \
> marge.sh
chmod +x marge.sh

ln -s /etc/marge/marge.sh marge

dh_make -y --indep --copyright mit --createorig

echo -e $INSTALL > debian/install
rm debian/*.ex
rm debian/*.EX
rm debian/marge-docs.docs
rm debian/README.source
rm debian/README.Debian
cp ../NOTICE debian/copyright
sed -i"" -e "s/^Depends: /Depends: $DEPENDENCIES, /" debian/control
sed -i"" -e "s/^Section: .*/Section: text/" debian/control
sed -i"" -e "s|^Homepage: .*|Homepage: $HOMEPAGE|" debian/control
sed -i"" -e "s/^Description: .*/Description: $DESCRIPTION_SHORT/" \
  debian/control
sed -i"" -e "s/^ <insert long description.*/ $DESCRIPTION_LONG/" \
  debian/control
sed -i"" -e "s/debhelper (>= 10)/debhelper (>= 9)/" debian/control
sed -i"" -e "s/unstable/$DISTRIBUTION/" debian/changelog
sed -i"" -e "s/marge (.*)/marge ($VERSION~$DISTRIBUTION$REVISION)/" \
  debian/changelog

echo "Please edit some config files now. For example:"
echo "marge-$VERSION~$DISTRIBUTION$REVISION/debian/changelog <-" \
  "leave a meaningful changelog message"
echo
echo "Afterwards, build the *.deb file by running 02_build_deb.sh"
